package me.bleidner.spring.model;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShoppingCart {

    private Long id;
    private final List<Long> products = new ArrayList<>();

    private final double internalId = Math.random();

    public void addProduct(long productId) {

        products.add(productId);
    }

    public void remove(long productId) {

        products.remove(productId);
    }
}
